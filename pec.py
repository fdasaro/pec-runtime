import argparse
import clingo
import subprocess
import itertools
from fractions import Fraction
import json
from datetime import datetime, timedelta
import zmq
import matplotlib.pyplot as plt
import random
import time
from pygame import mixer

import sys

# --------- PARSE COMMAND LINE INPUT ---------
parser = argparse.ArgumentParser()
parser.add_argument('path', help='path to the input file', type=str)
args = parser.parse_args()

# ----------------- GLOBAL VARIABLES -----------------
PATH_ = args.path

# IMPORTS DICTIONARY FROM XML
sys.path.append('C:/Users/Priscalab/Desktop/AVATEA/Interfaccia/')
from ConfigData import ConfigData
from Logger import Logger
from FileData import FileData
config_data_tree = ConfigData('conf.xml')
config_data_tree.set_path('C:/Users/Priscalab/Desktop/AVATEA/Interfaccia/conf/conf.xml')
c_data = config_data_tree.get_data_dictionary()
# startFile = FileData("conf/conf.xml")
childID = int(c_data['userID']['value'])

print(childID)

# Values in the causal domain implicity
# refer to the following standard frequency:
STD_FREQUENCY = 500

# PROBABILITY THRESHOLDS
TC_THRESHOLD_HIGHER = 0.66
TC_THRESHOLD_LOWER = 0.33
COG_THRESHOLD_HIGHER = 0.66
COG_THRESHOLD_LOWER = 0.33

# DIFFICULTY
DIFF_TIME_THRESHOLD = int(c_data['th_diff_time']['value']) # 10000 # DA INSERIRE IN INTERFACCIA # mseconds before difficulty change
MAX_DIFF_LEVEL = 0 # maximum difficulty level
MIN_DIFF_LEVEL = 0 # minimum difficulty level
INITIAL_DIFF_LEVEL = 0 
WAIT_BEFORE_AUTOMATIC_MODE = int(c_data['wait_auto_mode']['value']) # 20000# DA INSERIRE IN INTERFACCIA # mseconds before automatic mode is activated
MIN_DECISION_INTERVAL = int(c_data['wait_decision']['value']) # 1500 # DA INSERIRE IN INTERFACCIA # Minimum time interval between decisions (milliseconds)

# PATHS
TRANSLATOR_BIN = PATH_+"Translator/bin/win/translator.exe" 
PEC_DOMAIN_INDEPENDENT = PATH_+"pec.lp"
AUX_FILES_DIR = PATH_+"Auxiliary/"
AUX_DOMAIN = "auxiliary.lp"
CAUSAL_PART_PATH = PATH_+"Examples/AVATEA/Causal/avatea_v3.pec"
MUSIC_FOLDER = "C:/Users/Priscalab/Desktop/AVATEA/Suoni/Musiche/"
FX_FOLDER = "C:/Users/Priscalab/Desktop/AVATEA/Suoni/Effetti/"
LOGS_FOLDER = "C:/Users/Priscalab/Desktop/AVATEA/Logs/"

# DICTIONARY
JSON_TO_EPEC = {
        "H_Track": "hTracking",
        "V_Track": "vTracking",
        "Right_Pose" : "goodHeadPose",
        "Visible": "visible",
        "Valence": "valence",
        "S_Alignment": "goodShouldersAlignment"}

# ------------- END OF GLOBAL VARIABLES --------------

def set_diff_level(socket,difficulty_level):
    socket.send_json({ 'cmd': 'scale', 'param': 'max' })
    socket.send_json({ 'cmd': 'speed', 'param': '0.4' })
    socket.send_json({ 'cmd': 'background', 'param': 'hide' })
#    if (difficulty_level==0):
#        socket.send_json({ 'cmd': 'scale', 'param': 'max' })
#        socket.send_json({ 'cmd': 'speed', 'param': 'min' })
#        socket.send_json({ 'cmd': 'background', 'param': 'hide' })
#    elif (difficulty_level==1):
#        socket.send_json({ 'cmd': 'scale', 'param': 'min' })
#        socket.send_json({ 'cmd': 'speed', 'param': 'min' })
#        socket.send_json({ 'cmd': 'background', 'param': 'hide' })
#    elif (difficulty_level==2):
#        socket.send_json({ 'cmd': 'background', 'param': 'show' })
#        socket.send_json({ 'cmd': 'speed', 'param': 'min' })
#    elif (difficulty_level==3):
#        socket.send_json({ 'cmd': 'speed', 'param': 'max' })

def query(domain_description, query_str):
    ctr = clingo.Control()
    ctr.load(PEC_DOMAIN_INDEPENDENT)
    ctr.add("base", [], domain_description)
    ctr.add("base", [], query_str)
    ctr.ground([("base", [])])
    
    return extract_prob(ctr)

def extract_prob(ctr):
    ctr.configuration.solve.models = 0
    with ctr.solve(yield_=True) as handle:
        total_prob = 0
        for m in handle:
            current_trace_prob = 1
            for t in m.symbols(atoms=True):
                if (t.name=="effectChoice") or (t.name=="initialChoice"):
                    num = float(str(t.arguments[0].arguments[1].arguments[0]))
                    den = float(str(t.arguments[0].arguments[1].arguments[1]))
                    current_trace_prob = current_trace_prob * num/den
                elif (t.name=="eval"):
                    num = float(str(t.arguments[2].arguments[0]))
                    den = float(str(t.arguments[2].arguments[1]))
                    current_trace_prob = current_trace_prob * num/den
            
            total_prob = total_prob + current_trace_prob
            
    return total_prob

def extract_fluent_list(domain_description):
    # TODO: NOT EFFICIENT.
    # This should parse the input in a different way.
    ctr = clingo.Control()
    ctr.load(PEC_DOMAIN_INDEPENDENT)
    ctr.add("base", [], domain_description)
    ctr.ground([("base", [])])
    
    ctr.configuration.solve.models = 1
    
    fluents = []
    with ctr.solve(yield_=True) as handle:
        for m in handle:
            for t in m.symbols(atoms=True):
                if (t.name=="fluent"):
                    fluents.append(str(t.arguments[0]))
    
    return fluents

def construct_all_boolean_queries(fluents,instant):
    outcomes = []
    queries = []
    values = ["true","false"]
    
    for m in [list(zip(fluents,item)) for item in itertools.product(values, repeat=len(fluents))]:
        query = ""
        outcome = "({{"
        for f,v in m:
            outcome = outcome+f+"="+v+"," 
            query = query+"holds( ((" + f + "," + v + "),"+str(instant)+") ). "
        outcome = outcome[:-1] + "}},{prob})"
        outcomes.append(outcome)
        queries.append(query)

    return zip(outcomes,queries)

def translate_file(pec_domain_description_path):
    return (subprocess.run("bash -c %s < %s" % (TRANSLATOR_BIN,pec_domain_description_path), shell=True, stdout=subprocess.PIPE)).stdout.decode("utf-8")

def translate_str(pec_string):

    with open(AUX_FILES_DIR+AUX_DOMAIN, "w") as f:
        f.write(pec_string)

    return (subprocess.run("bash -c %s -m < %s" % (TRANSLATOR_BIN,AUX_FILES_DIR+AUX_DOMAIN), shell=True, stdout=subprocess.PIPE)).stdout.decode("utf-8")

def to_fraction_as_str(f):
    return str(Fraction(f).limit_denominator())

def construct_initial_prop(domain_description,outcomes_and_queries):
    
    iprop = "initially-one-of {"
    
    for o,q in outcomes_and_queries:
        outcome_probability = query(domain_description,q)
        if (outcome_probability > 0):
            iprop = iprop + o.format(prob=to_fraction_as_str(outcome_probability)) +","
            
    iprop = iprop[:-1] + "}"
    
    return iprop

def json_to_pec_narrative(message,normalisation_factor):
    
    pec_narrative = ""
    
    parsedInput = json.loads(message)
    for e in parsedInput:
        if float(parsedInput[e])>0:
            pec_narrative = pec_narrative + "{event} performed-at {{inst}} with-prob {prob} // {{timestamp}}\n".format(event=JSON_TO_EPEC[str(e)], prob=to_fraction_as_str(float(parsedInput[e])**(normalisation_factor)))
#            pec_narrative = pec_narrative + "{event} performed-at {{inst}} with-prob {prob} if-holds {{{{visible=true}}}} // {{timestamp}}\n".format(event=JSON_TO_EPEC[str(e)], prob=to_fraction_as_str(parsedInput[e]))
    return pec_narrative

def playmusic(character):
    if (character == 'first'):
        mixer.music.load(MUSIC_FOLDER+"coccinella.mp3")
    elif (character == 'second'):
        mixer.music.load(MUSIC_FOLDER+"coccinella.mp3")
    elif (character == 'third'):
        mixer.music.load(MUSIC_FOLDER+"pesciolino.mp3")
    elif (character == 'fourth'):
        # Default music if something goes wrong
        mixer.music.load(MUSIC_FOLDER+"ape.mp3")
    else:
        # Default music if something goes wrong
        mixer.music.load(MUSIC_FOLDER+"coccinella.mp3")
    mixer.music.play(loops=-1)

def playsound(character):
    if (character == 'first'):
        mixer.Channel(0).play(mixer.Sound(FX_FOLDER+"buzz.wav"))
    elif (character == 'second'):
        mixer.Channel(0).play(mixer.Sound(FX_FOLDER+"buzz.wav"))
    elif (character == 'third'):
        mixer.Channel(0).play(mixer.Sound(FX_FOLDER+"whistle.wav"))
    elif (character == 'fourth'):
        mixer.Channel(0).play(mixer.Sound(FX_FOLDER+"bubbles.wav"))
    else:
        mixer.Channel(0).play(mixer.Sound(FX_FOLDER+"laser.wav"))

def main():
    
    # --------- SOCKET INITIALISATION ---------
    ctx = zmq.Context()
    
    # Socket from Head Pose Recognition script:
    socket_head_pose = ctx.socket(zmq.SUB)
    socket_head_pose.setsockopt(zmq.LINGER,0)
    socket_head_pose.bind("tcp://*:5553")
    socket_head_pose.setsockopt(zmq.SUBSCRIBE,b'')
    
    # Socket to game:
    socket_game = ctx.socket(zmq.PUB)
    socket_game.setsockopt(zmq.LINGER, 0)  # All topics
    socket_game.bind("tcp://127.0.0.1:5554") # to PROTOM
    
    time.sleep(0.5) # Avoids Slow Joiner Syndrome

    # --------- PEC INITIALISATIONS ---------
    # fluents = extract_fluent_list(translated_domain)
    fluents = ['attention', 'taskCorrectness']
    actions = ['hTracking', 'vTracking', 'goodHeadPose', 'visible']
    
    output_narrative = ""
    current_instant = 0
    current_initial_prop = "initially-one-of { ({attention=true,taskCorrectness=true},1) }"
    max_instant_declaration = "\n#const maxinstant=2."

    translated_causal_part = translate_file(CAUSAL_PART_PATH)
    translated_causal_part = translated_causal_part + '\n'+'\n'.join(['action({}).'.format(action) for action in actions])
    
    # ------- OTHER INITIALISATIONS ----------
    # Music
    mixer.init()
    
    # Difficulty
    difficulty_level = INITIAL_DIFF_LEVEL
    possibly_decrease_difficulty = False
    possibly_increase_difficulty = False
    automatic_mode = False
    
    # Timers
    last_difficulty_reset = datetime.now()
    current_time = datetime.now()
    
    # Plots
    cogs = [1]
    tcs = [1]
    timestamps = []
    timestamps.append(current_time)
    ax = plt.axes()
    
    # Logger
    log_therapists = Logger()
    log_therapists.set_file_name(LOGS_FOLDER+str(childID)+"_per_terapisti.txt")
    
    log_pec = Logger()
    log_pec.set_file_name(LOGS_FOLDER+str(childID)+"_pec.pec")
    
    log_info = Logger()
    log_info.set_file_name(LOGS_FOLDER+str(childID)+"_config.txt")
    log_info.write(str(c_data))
    
    # START GAME WITH FOLLOWING PARAMETERS:
    curr_character = random.choice(['first','second','third','fourth'])
#    curr_character = random.choice(['fourth'])
    playmusic(curr_character)
    socket_game.send_json({ 'cmd': 'player', 'param': curr_character })
    # socket_game.send_json({ 'cmd': 'move', 'param': "start" }) # This is not needed as it is implicitly triggered
    socket_game.send_json({ 'cmd': 'loop', 'param': 'start' })
    socket_game.send_json({ 'cmd': 'direction', 'param': 'left_right' })
    set_diff_level(socket_game,difficulty_level)

    while True:
       message = (socket_head_pose.recv()).decode("utf-8")
       #print(message)

       if ("Quit" in json.loads(message)):
           break

       prev_time = current_time
       current_time = datetime.now()
       delta = (current_time-prev_time).total_seconds() * 1000

       current_narrative = json_to_pec_narrative(message, delta/STD_FREQUENCY)
       output_narrative = output_narrative + current_narrative.format(inst=current_instant,timestamp=current_time)
       translated_domain = translate_str(current_initial_prop) + translated_causal_part + translate_str(current_narrative.format(inst=0,timestamp="")) + max_instant_declaration

       cog = query(translated_domain, "holds( ((attention,true),0) ).")
       tc = query(translated_domain, "holds( ((taskCorrectness,true),0) ).")

       cogs.append(cog)
       tcs.append(tc)
       timestamps.append(current_time)
       if (not automatic_mode):
           # ------------------ CONTROLS START AND STOP (mode 1)-----------
    #       EPS = 0
    #       if (cogs[-1]>=cogs[-2]-cogs[-2]*EPS) and (tcs[-1]>=tcs[-2]-tcs[-2]*EPS):
    #           socket_game.send_json({ 'cmd': 'move', 'param': 'start' })
    #       else:
    #           socket_game.send_json({ 'cmd': 'move', 'param': 'stop' })
               
           # ------------------ CONTROLS START AND STOP (mode 2)-----------
           parsedInput = json.loads(message)
           if ((parsedInput["V_Track"] < 1) or (parsedInput["H_Track"] < 1) or (parsedInput["Right_Pose"] < 1) or (parsedInput["Visible"] < 1)):
                   print("stop")
                   socket_game.send_json({"cmd" : "move","param": "stop"})
           else:
               print("start")
               socket_game.send_json({"cmd" : "move", "param": "start"})
    
           # --------- FORWARDS DECISIONS ---------
           if (current_time - last_difficulty_reset > timedelta(milliseconds=MIN_DECISION_INTERVAL) ):
               
               last_difficulty_reset = datetime.now()
    
               if ((tc >= TC_THRESHOLD_HIGHER) and (cog >= COG_THRESHOLD_HIGHER)):
                   # Gradually make task harder
                   print("All good")
                   if (not possibly_increase_difficulty):
                       possibly_decrease_difficulty = False
                       possibly_increase_difficulty = True
                       change_difficulty_timer = current_time
                   elif ( current_time - change_difficulty_timer > timedelta(milliseconds=DIFF_TIME_THRESHOLD)) and (difficulty_level < MAX_DIFF_LEVEL):
                       change_difficulty_timer = current_time
                       difficulty_level = difficulty_level+1
                       set_diff_level(socket_game,difficulty_level)
               elif (( cog >= COG_THRESHOLD_HIGHER ) and (tc < TC_THRESHOLD_HIGHER) and (tc >= TC_THRESHOLD_LOWER)):
                   # Sound
                   print("Sound")
                   playsound(curr_character)
                   socket_game.send_json({ 'cmd': 'visual_stimulus', 'param': 'start' })
               elif (( tc >= TC_THRESHOLD_HIGHER ) and (cog < COG_THRESHOLD_HIGHER) and (cog >= COG_THRESHOLD_LOWER)):
                   # Visual stymulus
                   print("Visual stymulus")
                   socket_game.send_json({ 'cmd': 'visual_stimulus', 'param': 'start' })
               elif ((tc < TC_THRESHOLD_HIGHER) and (tc >= TC_THRESHOLD_LOWER) and (cog < COG_THRESHOLD_HIGHER) and (cog >= COG_THRESHOLD_LOWER)):
                   # Make task easier + Visual stymulus
                   print("Make task easier + Visual stymulus")
                   socket_game.send_json({ 'cmd': 'visual_stimulus', 'param': 'start' })
                   
                   if (not possibly_decrease_difficulty):
                       possibly_increase_difficulty = False
                       possibly_decrease_difficulty = True
                       change_difficulty_timer = current_time
                   elif ( current_time - change_difficulty_timer > timedelta(milliseconds=DIFF_TIME_THRESHOLD)) and (difficulty_level > MIN_DIFF_LEVEL):
                       print("lower_diff")
                       change_difficulty_timer = current_time
                       difficulty_level = difficulty_level-1
                       set_diff_level(socket_game,difficulty_level)
               else:
                   # Make task easier + Sound + Visual stymulus
                   print("Make task easier + Sound + Visual stymulus")
                   socket_game.send_json({ 'cmd': 'visual_stimulus', 'param': 'start' })
                   playsound(curr_character)
                   
                   if (not possibly_decrease_difficulty):
                       possibly_increase_difficulty = False
                       possibly_decrease_difficulty = True
                       change_difficulty_timer = current_time
                   elif ( current_time - change_difficulty_timer > timedelta(milliseconds=WAIT_BEFORE_AUTOMATIC_MODE)):
                       automatic_mode = True
                       socket_game.send_json({ 'cmd': 'move', 'param': "start" })
                       socket_game.send_json({ 'cmd': 'loop', 'param': "start" })
                       set_diff_level(socket_game,INITIAL_DIFF_LEVEL)
                   elif ( current_time - change_difficulty_timer > timedelta(milliseconds=DIFF_TIME_THRESHOLD)) and (difficulty_level > MIN_DIFF_LEVEL):
                       print("lower_diff")
                       change_difficulty_timer = current_time
                       difficulty_level = difficulty_level-1
                       set_diff_level(socket_game,difficulty_level)
       elif ((tc >= TC_THRESHOLD_HIGHER) and (cog >= COG_THRESHOLD_HIGHER)):
           automatic_mode = False

       # --------- PREPARE FOR NEXT ITERATION ---------
       current_initial_prop = construct_initial_prop(translated_domain,construct_all_boolean_queries(fluents,1))
       current_instant = current_instant+1

    print("------------------ END OF SESSION ------------------")
    
    # ------------ EXIT PROCEDURES AND LOGGING ------------
    mixer.music.stop()

    log_pec.write(output_narrative)

    ax.plot(cogs, label='Cognitive Attention')
    ax.plot(tcs, label='Behavioral Attention')
    ax.legend()
    plt.draw()
    
    min_cog_index=0; max_cog_index=0;
    min_tc_index=0; max_tc_index=0;
    total_high_cog_time = 0
    total_high_tc_time = 0
    
    current_interval_min=0; current_interval_max=0
    for i in range(1,len(cogs)):
        if (cogs[i] >= COG_THRESHOLD_HIGHER) and (i==current_interval_max+1):
            current_interval_max = i
            if (current_interval_max - current_interval_min > max_cog_index - min_cog_index):
                min_cog_index = current_interval_min
                max_cog_index = current_interval_max
            total_high_cog_time = total_high_cog_time + (timestamps[i]-timestamps[i-1]).total_seconds()
        else:
            current_interval_min = i
            current_interval_max = i
            
    current_interval_min=0; current_interval_max=0
    for i in range(1,len(tcs)):
        if (tcs[i] >= TC_THRESHOLD_HIGHER) and (i==current_interval_max+1):
            current_interval_max = i
            if (current_interval_max - current_interval_min > max_tc_index - min_tc_index):
                min_tc_index = current_interval_min
                max_tc_index = current_interval_max
            total_high_tc_time = total_high_tc_time + (timestamps[i]-timestamps[i-1]).total_seconds()
        else:
            current_interval_min = i
            current_interval_max = i
            
    total_time = (timestamps[-1]-timestamps[0]).total_seconds()
    
    f_data = FileData(c_data['last_user']['value'])
    f_data.set_path('C:/Users/Priscalab/Desktop/AVATEA/Interfaccia/data/'+c_data['last_user']['value'])
    f_data.create_data_by_name('tempo_sessione_sec','{:.1f}'.format(total_time))
    f_data.create_data_by_name('attenzione_visiva_sec','{:.1f}'.format(total_high_cog_time))
    f_data.create_data_by_name('sequenza_attenzione_visiva_sec','{:.1f}'.format((timestamps[max_cog_index]-timestamps[min_cog_index]).total_seconds()))
    f_data.create_data_by_name('postura_corretta_sec','{:.1f}'.format(total_high_tc_time))
    f_data.create_data_by_name('sequenza_postura_corretta_sec','{:.1f}'.format((timestamps[max_tc_index]-timestamps[min_tc_index]).total_seconds()))
    f_data.write_data()
    
    log_therapists.write("Tempo totale sessione: {:.1f}sec".format(total_time))
    
    #print("Highest Level of Cognitive Attention held for: "+str(total_high_cog_time)+"sec (Longest streak: "+str((timestamps[max_cog_index]-timestamps[min_cog_index]).total_seconds())+"sec) from "+str(timestamps[min_cog_index])+" (index "+str(min_cog_index)+") to "+str(timestamps[min_cog_index])+" (index "+str(max_cog_index)+")")
    log_therapists.write("Attenzione visiva mantenuta per: {:.1f}sec - {:.2f}\% (Sequenza più lunga: {:.1f}sec, da {} (index {}) a {} (index {}))".format(total_high_cog_time, total_high_cog_time/total_time*100, (timestamps[max_cog_index]-timestamps[min_cog_index]).total_seconds(), str(timestamps[min_cog_index].strftime("%H:%M:%S")), min_cog_index, str(timestamps[max_cog_index].strftime("%H:%M:%S")), max_cog_index))
    #print("Highest Level of Behavioral Attention held for: "+str(total_high_tc_time)+"sec (Longest streak: "+str((timestamps[max_tc_index]-timestamps[min_tc_index]).total_seconds())+"sec) from "+str(timestamps[min_tc_index])+" (index "+str(min_tc_index)+") to "+str(timestamps[min_cog_index])+" (index "+str(max_tc_index)+")")
    log_therapists.write("Postura corretta mantenuta per: {:.1f}sec - {:.2f}\% (Sequenza più lunga: {:.1f}sec, da {} (index {}) a {} (index {}))".format(total_high_tc_time, total_high_tc_time/total_time*100, (timestamps[max_tc_index]-timestamps[min_tc_index]).total_seconds(), str(timestamps[min_tc_index].strftime("%H:%M:%S")), min_tc_index, str(timestamps[max_tc_index].strftime("%H:%M:%S")), max_tc_index))


# CALLS MAIN FUNCTION
if __name__ == "__main__":
	main()
