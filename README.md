# PEC-ASP (runtime version)

The Probabilistic Event Calculus (PEC) is a probabilistic extension of a popular framework for reasoning about the effects of a narrative of action occurrences (events) along a time line known as the [Event Calculus](https://en.wikipedia.org/wiki/Event_calculus) (EC).

This repository hosts an [ASP](https://en.wikipedia.org/wiki/Answer_set_programming) (clingo) implementation of a runtime PEC able to perform temporal projection.

You can find more details about PEC in the associated [paper](https://link.springer.com/chapter/10.1007/978-3-319-61660-5_7), which contains a formal description of the syntax used by this implementation.

# Compile and run under Windows

This runtime version is for Windows operating systems only. However, it can be tweaked so as to make it work under Linux and macOS as well. This version requires cygwin or MinGW with the libraries [flex](https://github.com/westes/flex), [Bison](https://www.gnu.org/software/bison/) and the [gcc](https://gcc.gnu.org) compiler.

The translator can be compiled by navigating to the `Translator` directory, and then using the following command:

``` sh
bison -d grammar.y; flex lexer.l; gcc dynamicarray.c lex.yy.c grammar.tab.c -o Path_to_translator/translator
```

# Known issues
The translator seems to have problems with assignments of length 21, e.g.

```
{ aaaaaaaaaaaaaaaaa=eeee }
        causes-one-of
                { ({verticalTraceeaa=false},1/1) }
```

translates to

```
belongsTo((verticalTraceeaa,false)3, id0).
causedOutcome( (id0, frac(1,1)), I) :-
        holds( ((aaaaaaaaaaaaaaaaa,eeee)3,I) ).
```